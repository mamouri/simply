import React from 'react';
import {IFund, IHolding} from './interface';
import {Asset} from './Asset';
import {Holdings} from './Holdings';
import ReactLoading from 'react-loading';

interface FundProps {
    isLoading: boolean,
    error: object | null,
    data: {
        fund: IFund | null
        holdings: IHolding[] | null
    },
}

export const Fund = (props: FundProps) => {

    const fundSummary = (fund: IFund) =>
        <div>
            <h1>{fund.name}</h1>

            <h2>
                Average Market Cap:{' '}
                {fund.averageMarketCap.toLocaleString('en-US',
                    {style: 'currency', currency: 'AUD'}
                )}
            </h2>

            <Asset assets={fund.assets || null}/>

            <Holdings holdings={props.data ? props.data.holdings : null}/>
        </div>;

    return (
        <div>
            {props.isLoading && <ReactLoading type='balls'/>}

            {props.data && props.data.fund ?
                fundSummary(props.data.fund) :
                !props.isLoading && <div style={{padding: '20px 0 0 0'}}>The fund was not found!</div>
            }
        </div>
    );
};
