import React from 'react';
import {IHolding} from './interface';

interface HoldingsProps {
    holdings: IHolding[] | null
}

export const Holdings = (props: HoldingsProps) =>
    <div>
        <h2>Company's Holding</h2>
        <table>
            <thead>
            <tr>
                <th>Company Name</th>
                <th>Symbol</th>
                <th>% Portfolio Weight</th>
                <th>Shares Owned</th>
                <th>Sector</th>
                <th>Country</th>
                <th>First Bought</th>
                <th>YTD Return</th>
            </tr>
            </thead>
            <tbody>
            {props.holdings ? props.holdings.map(
                (h: IHolding, idx) =>
                    <tr key={idx}>
                        <td><a href={h.url}>{h.company}</a></td>
                        <td>{h.symbol}</td>
                        <td>{h.portfolioWeight}</td>
                        <td>{h.sharesOwned}</td>
                        <td>{h.sector}</td>
                        <td>{h.country}</td>
                        <td>{(new Date(h.firstBought)).toLocaleDateString('en-AU')}</td>
                        <td>{h.ytdReturn}</td>
                    </tr>
            ) : null}
            </tbody>
        </table>
    </div>;