export interface IAsset {
    type: string;
    amount: number;
}

export interface IFund {
    id: string;
    name: string;
    assets?: IAsset[];
    averageMarketCap: number;
}

export interface IHolding {
    fundId: string;
    company: string;
    symbol: string;
    country: string;
    url: string;
    portfolioWeight: number;
    sharesOwned: number;
    sector: string;
    firstBought: string;
    ytdReturn: number;
}

export interface FundHoldings {
    fund: IFund;
    holdings: IHolding[]
}

export interface MetadataObj {
    [key: string]: any
}
