import React from 'react';
import {IAsset} from './interface';

interface AssetProps {
    assets: IAsset[] | null;
}

export const Asset = (props: AssetProps) =>
    <table>
        <thead>
        <tr>
            <th>Asset type</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        {props.assets ?
            props.assets.map((asset: IAsset, idx) =>
                <tr key={idx}>
                    <td>{asset.type}</td>
                    <td>{asset.amount}</td>
                </tr>
            ) : null
        }
        </tbody>
    </table>;