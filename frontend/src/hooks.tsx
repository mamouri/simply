import React, {Component} from 'react';
import axios from 'axios';
import * as R from 'ramda';

export const withFetching = (url: string, remotePath: string[], propName: string = 'data') => (Component: any) =>
    class WithFetching extends React.Component {
        constructor(props: any) {
            super(props);

            this.state = {
                [propName]: null,
                isLoading: false,
                error: null,
            };
        }

        componentDidMount() {
            this.setState({isLoading: true});

            axios.get(url)
                .then(result => this.setState({
                    [propName]: R.path(remotePath, result.data),
                    isLoading: false
                }))
                .catch(error => this.setState({
                    error,
                    isLoading: false
                }));
        }

        render() {
            return <Component {...this.props} {...this.state} />;
        }
    };
