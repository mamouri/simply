import React, {Component} from 'react';
import {Fund} from './components/Fund';
import {withFetching} from './hooks';
import './App.css';

interface Props {

}

interface State {
    currentFund: string | null,
    submitted: boolean
}

class App extends Component<Props, State> {
    textInput: React.RefObject<any>;

    constructor(props: any) {
        super(props);

        this.state = {currentFund: null, submitted: false};

        this.textInput = React.createRef();
    }

    render() {
        const baseURL = 'http://localhost:3000/scrape/';

        const FundWithFetching = withFetching(baseURL + this.state.currentFund, ['payload'])(Fund);

        const searchClick = () => {
            this.setState({currentFund: this.textInput.current.value, submitted: true});
        };

        return (
            <div className="App">
                <div>
                    <label htmlFor="male">Fund Name</label><br />
                    <input style={{width: '50%', padding: '10px'}} type="input" name="fundName" ref={this.textInput} />

                    <button style={{padding: '10px'}} onClick={searchClick.bind(this)}>Search</button>
                </div>

                {this.state.submitted && <FundWithFetching />}
            </div>
        );
    }
}

export default App;
