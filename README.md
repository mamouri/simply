## Intro
Coding challenge of Simply Wall Street


## Requirements
+ Docker

## Installation

To run the backend and frontend use docker-compose command:

        make up

Alternatively, you may run the projects manually. First install the dependencies of both projects using `yarn install`.

Run the backend server using the following command:

        yarn run watch-server

Run the frontend server using the following command:

        yarn start

By default the backend and frontend servers run on port 3000 and 5000 respectively.


## CLI Usage:
The backend server comes with a CLI. You may run it using the following command:

        yarn cli voo