const program = require('commander');
const scraper = require('./src/scraper.ts');
const Table = require('cli-table');

async function download_funds(names: string[]) {

    const missings: string[] = [];

    const promises: Promise<string[]>[] = names.map(async (name: string) => {
        try {
            const fund = await scraper(name);

            return [fund.id, fund.name, fund.average_market_cap];
        } catch {
            missings.push(name);
            return null;
        }
    });

    Promise.all(promises)
        .then((results: string[][]) => {
            const table = new Table({head: ['ID', 'Name', 'Average Market Cap'], colWidths: [10, 100, 20]});
            table.push(...results);
            console.log(table.toString());

            if (missings) {
                console.log(`The following funds were not found: ${missings.join(', ')}`);
            }
        })
        .catch((e) => console.log);
}

program
    .version('0.1.0')
    .arguments('<fund...>')
    .action(async (funds: string[]) => {
        await download_funds(funds)
    })
    .parse(process.argv);
