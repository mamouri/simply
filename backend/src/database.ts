import * as sqlite from 'sqlite';
import * as path from 'path';

const dbPromise = Promise.resolve()
    .then(() => sqlite.open('./scraper.db', { promise: Promise }))
    .then(db => db.migrate({migrationsPath: path.join(__dirname, '/migrations')}));

export = dbPromise;