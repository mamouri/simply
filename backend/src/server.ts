import * as Koa from 'koa';
import * as Router from 'koa-router';

const Cors = require('@koa/cors');
const Json = require('koa-json');
const scraper = require('./scraper');

const app = new Koa();
const router = new Router();

router.get('/scrape/:fund', async (ctx) => {
    const fund = ctx.params.fund;

    let payload: object | null = null;
    let errors: string[] | null = null;

    try {
        payload = await scraper(fund);
    } catch (e) {
        console.error(e);
        errors = ['The fund does not exists!']
    }

    ctx.body = {errors, payload};
});

app
    .use(Json({}))
    .use(Cors())
    .use(router.routes())
    .use(router.allowedMethods())
    .use((ctx, next) => {
        return next().catch(err => {
            ctx.status = err.status || 500;
            ctx.body = {
                error: err.message
            };
        });
    });

app.listen(3000);

console.log('Server running on port 3000');