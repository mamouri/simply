-- Up
CREATE TABLE IF NOT EXISTS fund (
    id text unique,
    created_at datetime,
    name text,
    asset json,
    average_market_cap text
);

CREATE TABLE IF NOT EXISTS holding (
    fund_id text,
    company text,
    symbol text,
    country text,
    url text,
    portfolio_weight real,
    shares_owned integer,
    sector text,
    first_bought date,
    ytd_return date
)

-- Down
DROP TABLE fund;
DROP TABLE holdings;