import * as dbPromise from './database'
import {Fund, Holding, FundHoldings, Asset, MetadataObj} from './scraper.d';

const request = require('request-promise-native');
const cheerio = require('cheerio');
const jsonframe = require('jsonframe-cheerio');

/**
 * Parse a page and extract the desired entities using jQuery selectors
 *
 * @param html: The content of the page
 * @param frame: jsonframe of the list of entities to be extracted
 * @param container: jQuery selector of the container element
 */
function findEntities(html: string, frame: object, container: string = 'body'): MetadataObj {
    const $ = cheerio.load(html);
    jsonframe($);

    return $(container).scrape(frame, {object: true})
}

/**
 * Clean a string and parse it to integer
 * @param s
 */
function cleanParseFloat(s: string): number {
    return parseFloat(
        s.replace(',', '')
            .replace(/\W+/, '')
            .replace(/Mil/i, '000000')
    );
}


/**
 * Extract fund information from the website
 * @param fund
 */
async function extractFundEntities(fund: string) {
    const frame = {
        'name': '.r_title h1',
        'asset': {
            '_s': '#asset_allocation_tab tbody tr:not(.hr)',
            '_d': [{
                'name': '.row_lbl',
                'amount': 'td:nth-child(3)',
            }]
        },
        'average_market_cap': '#equity_style_tab .col3 .col1:nth-child(1) .r_txt3',
    };

    const response = await request('http://portfolios.morningstar.com/fund/summary?t=' + fund,
        {resolveWithFullResponse: true}
    );

    // Unfortunately with Morning Star, there is no easy way to detect either:
    //      a. The fund does not exists
    //      b. The server is down.
    //
    // Even worse, the HTTP status code of both success and failed requests are always 200!
    if (response.req.path === '/back_soon.html')
        throw Error('The fund does not exists');

    const data = findEntities(response.body, frame);

    const cleanupAsset = (primitive: MetadataObj): Asset => {
        return {
            type: String(primitive.name),
            amount: parseFloat(String(primitive.amount)),
        }
    };

    const cleanUpFund = (primitive: MetadataObj): Fund => {
        return {
            id: fund,
            name: primitive.name,
            assets: primitive.asset.map(cleanupAsset),
            averageMarketCap: cleanParseFloat(primitive.average_market_cap)
        }
    };

    return cleanUpFund(data);

}

async function extractHoldingsApi(fund: string): Promise<string> {
    const res = await request('http://portfolios.morningstar.com/fund/holdings?t=' + fund);

    const partial_url = /holdings_tab\?&t=(.*)(?=&callback)/gm.exec(res);

    return 'http://portfolios.morningstar.com/portfo/fund/ajax/' + partial_url[0];
}

async function extractHoldingsEntities(fund: string): Promise<Holding[]> {
    const frame = {
        'holdings': {
            '_s': '#holding_epage0 tr:not(.hr)',
            '_d': [{
                'name': '.row_lbl',
                'country': 'td:nth-child(13)',
                'url': 'th a @ href',
                'portfolio_weight': 'td:nth-child(5)',
                'shares_owned': 'td:nth-child(6)',
                'sector': 'td:nth-child(8) span @ class',
                'first_bought': 'td:nth-child(11)',
                'ytd_return': 'td:nth-child(14)',
            }]
        }
    };

    const sectorMapping: MetadataObj = {
        'sctr_bm': 'Basic Materials',
        'sctr_cc': 'Consumer Cyclical',
        'sctr_fs': 'Financial Services',
        'sctr_re': 'Real Estate',
        'sctr_cs': 'Communication Services',
        'sctr_e': 'Energy',
        'sctr_i': 'Industrials',
        'sctr_t': 'Technology',
        'sctr_cd': 'Consumer Defensive',
        'sctr_h': 'Healthcare',
        'sctr_u': 'Utilities',
    };

    const cleanupHolding = (primitive: MetadataObj): Holding => {
        return {
            fundId: fund,
            company: primitive.name,
            symbol: /t=(.*?)(?=&)/.exec(primitive.url)[1],
            country: primitive.country,
            url: primitive.url,
            portfolioWeight: parseFloat(primitive.portfolio_weight),
            sharesOwned: cleanParseFloat(primitive.shares_owned),
            sector: sectorMapping[primitive.sector],
            firstBought: new Date(primitive.first_bought),
            ytdReturn: parseFloat(primitive.ytd_return),
        }
    };


    const url = await extractHoldingsApi(fund);

    const html = JSON.parse(await request(url)).htmlStr;

    const data = findEntities(html, frame, '#equity_tab').holdings;

    return data.map(cleanupHolding);
}

async function storeFundInDatabase(fund: Fund, holdings: Holding[]): Promise<FundHoldings> {
    const update_fund = `INSERT OR REPLACE INTO fund(id, created_at, name, asset, average_market_cap)
        VALUES(?, CURRENT_TIMESTAMP, ?, ?, ?)`;

    const remove_holdings = `DELETE FROM holding WHERE fund_id=$id`;
    const insert_holdings = `INSERT INTO holding(fund_id, company, symbol, country, url, portfolio_weight, shares_owned, sector, first_bought, ytd_return)
    VALUES($fund_id, $company, $symbol, $country, $url, $portfolio_weight, $shares_owned, $sector, $first_bought, $ytd_return);`;

    const db = await dbPromise;

    await db.all(update_fund, fund.id, fund.name, JSON.stringify(fund.assets), fund.averageMarketCap);
    await db.all(remove_holdings, fund.id);

    const promises = holdings.map(async h => await db.all(insert_holdings,
        h.fundId, h.company, h.symbol, h.country, h.url, h.portfolioWeight, h.sharesOwned, h.sector, h.firstBought, h.ytdReturn
    ));

    await Promise.all(promises);

    return {fund, holdings}
}

/**
 * Load fund and its holding from database
 *
 * @param fundName
 */
async function loadFundFromDatabase(fundName: string): Promise<FundHoldings> {
    const selectFund = `SELECT *
        FROM fund
        WHERE id=$id and created_at >= DATETIME(CURRENT_TIMESTAMP, "-5 days");`;

    const selectHoldings = `SELECT * FROM holding WHERE fund_id=$id`;

    const dbAssetTransformer = (d: MetadataObj): Asset => ({
        type: d.type,
        amount: d.amount,
    });

    // TODO: Consider interpolating db object to TypeScript definitions automatically using an ORM
    const dbFundTransformer = (d: MetadataObj): Fund => ({
        id: dbFund.id,
        name: dbFund.name,
        assets: JSON.parse(dbFund.asset).map(dbAssetTransformer),
        averageMarketCap: cleanParseFloat(d.average_market_cap)
    });

    const dbHoldingTransformer = (d: MetadataObj): Holding => ({
        fundId: d.fund_id,
        company: d.company,
        symbol: d.symbol,
        country: d.country,
        url: d.url,
        portfolioWeight: d.portfolio_weight,
        sharesOwned: d.shares_owned,
        sector: d.sector,
        firstBought: new Date(d.first_bought),
        ytdReturn: d.ytd_return,
    });


    const db = await dbPromise;

    const dbFund: MetadataObj = await db.get(selectFund, fundName);
    const fund = dbFundTransformer(dbFund);

    const dbHoldings: MetadataObj[] = await db.all(selectHoldings, fundName);
    const holdings: Holding[] = dbHoldings.map(dbHoldingTransformer);

    return {fund, holdings}
}

/**
 * Rudimentary scraper function with memorization support
 * @param fundName
 */
async function scraper(fundName: string): Promise<FundHoldings> {

    let fundHoldings = null;

    try {
        fundHoldings = await loadFundFromDatabase(fundName);
    } catch {
        const fundEntities = await extractFundEntities(fundName);
        const holdingsEntities = await extractHoldingsEntities(fundName);

        fundHoldings = await storeFundInDatabase(fundEntities, holdingsEntities);
    }

    return fundHoldings;
}

export = scraper;