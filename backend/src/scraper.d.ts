declare module 'scraper' {
    export interface scraper {
        fund: string;
    }
}

export interface Asset {
    type: string;
    amount: number;
}

export interface Fund {
    id: string;
    name: string;
    assets: Asset[];
    averageMarketCap: number;
}

export interface Holding {
    fundId: string;
    company: string;
    symbol: string;
    country: string;
    url: string;
    portfolioWeight: number;
    sharesOwned: number;
    sector: string;
    firstBought: Date;
    ytdReturn: number;
}

export interface FundHoldings {
    fund: Fund;
    holdings: Holding[]
}

interface MetadataObj {
    [key: string]: any
}
